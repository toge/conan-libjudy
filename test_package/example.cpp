#include <iostream>
#include "Judy.h"


static Word_t set1[] = { 
    0L,
    5L,
    6L,
    7L,
    1024L,
    11111L,
    65534L,
    65535L,
    65536L,
    555555L,
    ULONG_MAX
};

int main() {
    Word_t    i;
    void     *PJArray = 0;
    JError_t  JError;

    for (i = 0L; i < sizeof(set1) / sizeof(set1[0]); i++)
    {
        if (Judy1Set(&PJArray, set1[i], &JError) == JERR)
        {
            return 1;
        }
    }

    return 0;
}

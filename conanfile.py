from conans import ConanFile, CMake, tools
import shutil

class LibjudyConan(ConanFile):
    name        = "libjudy"
    version     = "1.0.5"
    license     = "LGPL 2.1"
    author      = "toge.mail@gmail.com"
    url         = "https://bitbucket.org/toge/conan-libjudy"
    description = "General purpose dynamic array"
    topics      = ("dynamic array", "large data")
    settings    = "os", "compiler", "build_type", "arch"
    generators  = "cmake"
    exports     = "CMakeLists.txt"

    def source(self):
        tools.get("https://downloads.sourceforge.net/project/judy/judy/Judy-1.0.5/Judy-{}.tar.gz".format(self.version))
        shutil.move("judy-{}".format(self.version), "judy")
        shutil.copy("CMakeLists.txt", "judy/src/CMakeLists.txt")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="judy/src")
        cmake.build()

    def package(self):
        self.copy("Judy.h",  dst="include", src="judy/src", keep_path=False)
        self.copy("*.lib",   dst="lib",     keep_path=False)
        self.copy("*.dll",   dst="bin",     keep_path=False)
        self.copy("*.so",    dst="lib",     keep_path=False)
        self.copy("*.dylib", dst="lib",     keep_path=False)
        self.copy("*.a",     dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["judy"]

